# GCR - Google Cloud Registry

## gcloud commands

### Remove an image

gcloud container images delete {zone}.gcr.io/{project}/{folder} --project={project} --quiet